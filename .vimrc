
set nocompatible              " be iMproved, required
filetype off                  " required
syntax on

"Plugin 'Valloric/YouCompleteMe'
"Plugin 'leafgarland/typescript-vim'
"Plugin 'terryma/vim-multiple-cursors'
"Plugin 'vim-syntastic/syntastic'
"Plugin 'editorconfig/editorconfig-vim'
"Plugin 'othree/yajs.vim'
"Plugin 'mhinz/vim-signify'

" Source https://github.com/tmux/tmux/issues/1246
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

so ~/.vim/coc.vim

set backspace=2 " Backspace EVERYTHING
set t_Co=256
set laststatus=2
let g:lightline = {
  \ 'active': {
  \  'left': [ [ 'mode', 'paste' ],
  \            [ 'readonly', 'relativepath', 'modified' ]
  \          ]
  \ },
  \ 'inactive': {
  \  'left': [ [ 'relativepath' ] ]
  \ }
  \}
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

" Vim Options
let g:mymouse = "a"
let g:mylistchars = "tab:>-,space:˽,trail:*"
let g:colorcolumn=80

" Highlight trailing whitespace
" see https://vim.fandom.com/wiki/Highlight_unwanted_spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Fix pink background on pmenu (makes text unreadable)
highlight Pmenu guibg=#444444

" Mouse 220 column fix
if has("mouse_sgr")
    set ttymouse=sgr
else
    set ttymouse=xterm2
end

set hlsearch
set background=dark
set number
set foldmethod=indent
set foldlevel=20
set autoindent
set backup
set confirm
"set cursorline
set cursorcolumn
set ruler
set smartindent
"set spell
set splitright
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
let &colorcolumn=g:colorcolumn
set list
let &mouse= g:mymouse
"set mouse?
let &listchars = g:mylistchars
"set listchars?
" Clear last search highlighting
map <Space> :noh<cr>
map , :call ToggleCopyable()<cr>

let g:copyable=0

function! ToggleCopyable()
  if g:copyable
    let &mouse= g:mymouse
    let &listchars = g:mylistchars
    let &colorcolumn=g:colorcolumn
    set number
    set nopaste
    set signcolumn=yes
    let g:copyable=0
  else
    set mouse=
    set listchars=tab:\ \ 
    set colorcolumn=
    set nonumber
    set paste
    set signcolumn=no
    let g:copyable=1
  endif
endfunction
