if [ -d ~/bin ]; then
  export PATH=$(realpath ~/bin):$PATH
fi

if [ -d ~/.bashrcdir ] && compgen -G ~/.bashrcdir/*.bash > /dev/null; then
  for f in `ls ~/.bashrcdir/*.bash`; do
    . $f
  done
fi

