#!/bin/bash

function docker-bash {
  local NAME_START=224
  local ID_END=13
  if [ $# -ne 1 ]; then
    echo "docker-bash"
    echo ""
    echo "Connect to a docker containers (bash) terminal"
    echo ""
    echo "usage: docker-bash <container_name>"
    return 1
  fi

  local ID=$(docker ps | tail -n +2 | cut -c "-${ID_END},${NAME_START}-" | grep " $1\$" | cut -d ' ' -f 1)
  if [ "$ID" == "" ]; then
    echo "Unknown docker container name"
    return 2
  fi

  docker exec -it $ID bash
}

function _docker-bash-completion {
  local NAME_START=224
  local cur=${COMP_WORDS[COMP_CWORD]}
  COMPREPLY=( $(compgen -W "`docker ps | tail -n +2 | cut -c "${NAME_START}-"`" -- $cur) )
}
complete -F _docker-bash-completion docker-bash

