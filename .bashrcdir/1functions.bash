function nvm_use {
  if [ -f .nvmrc ]; then
    file='.nvmrc';
  else
    folder=`git rev-parse --show-toplevel 2> /dev/null`
    if [ -f $folder/.nvmrc ]; then
      file="$folder/.nvmrc"
    else
      return
    fi
  fi

  version="v`cat $file`"
  if [ `node --version` != $version ]; then
    echo switching version to $version

    nvm ls $version 2>&1 >/dev/null

    if [ $? -ne 0 ]; then
      echo Version not currently installed. Trying to install
      nvm install $version
    fi

    nvm use $version
  fi
}

nvm_use

function cd {
  builtin cd "$@"
  if [ "$NVMUSE" == "1" ]; then
    nvm_use
  fi
}

function dcd {
  cd ~/Development/$1
}

_dcd-completion() {
  local cur=${COMP_WORDS[COMP_CWORD]}
  local dir=~/Development

  COMPREPLY=()
  k=0
  owd=$(pwd)
  cd "$dir"
  for j in $( compgen -f "$cur" ); do
    [ -d "$j" ] && j="${j}/" || continue
    COMPREPLY[k++]=${j}
  done
  cd "$owd"
  return 0
}
complete -o nospace -F _dcd-completion dcd

function lgrep {
  if [ $# -eq 1 ]; then
    grep --exclude='*~' --exclude-dir='node_modules' --exclude-dir='.git' -I --color=always -rn "$1" | cut -c -300 | less -rF
  else
    grep --exclude='*~' --exclude-dir='node_modules' --exclude-dir='.git' -I --color=always -rn "$@" | cut -c -300 | less -rF
  fi
}

function gim {
  local file=`echo $1 | cut -d : -f 1`
  local line=`echo $1 | cut -d : -f 2`
  vim $file +$line
}

function addpath {
  if [ $# -ne 1 ]; then
    echo "usage: addpath <path>"
    return
  fi

  export PATH="$1:$PATH"
}

function jsonpp {
  cat $1 | jq -C .
}

function mdhtml {
  if [ $# -ne 1 ]; then
    echo "usage: mdhtml <markdown_path>"
    return 1
  fi

  showdown -m makehtml -i $1 | lynx -stdin
}

function npr {
  if [ $# -ne 1 ]; then
    echo "usage: mdhtml <npm package>"
    return 1
  fi

  local NPM_ROOT=$(npm root)

  if [ ! $? ]; then
    echo "Could not find node modules directory. Are you in an npm project?"
    return 2
  fi

  local DIR="$NPM_ROOT/$1"

  if [ ! -e $DIR ]; then
    echo "Could not find module directory. Is it installed?"
    return 3
  fi

  local README=$(find $DIR -iname readme.md)

  if [ ! $? ]; then
    echo "Couldn't find a readme. Is there one?"
    return 4
  fi

  mdhtml $README
}

_npr_completion() {
  local NPM_ROOT=$(npm root)
  local cur=${COMP_WORDS[COMP_CWORD]}

  if [ ! $? ]; then
    return 1
  fi

  COMPREPLY=( $(compgen -W "`ls -p $NPM_ROOT`" -- $cur) )
}

complete -F _npr_completion npr

function nets {
  if which netstat >/dev/null; then
    echo netstat -nlp --inet --inet6
    netstat -nlp --inet --inet6
  elif which ss > /dev/null; then
    echo ss -lntup
    ss -lntup
  else
    echo No netstat program installed
    exit 1
  fi
}

function snets {
  if which netstat >/dev/null; then
    echo sudo netstat -nlp --inet --inet6
    sudo netstat -nlp --inet --inet6
  elif which ss > /dev/null; then
    echo sudo ss -lntup
    sudo ss -lntup
  else
    echo No netstat program installed
    exit 1
  fi
}
