if [ -f ~/.hostColors ]; then
  . ~/.hostColors
fi

_setColors() {
  ### Colors to Vars ### {{{
  ## Inspired by http://wiki.archlinux.org/index.php/Color_Bash_Prompt#List_of_colors_for_prompt_and_Bash
  ## Terminal Control Escape Sequences: http://www.termsys.demon.co.uk/vtansi.htm
  ## Consider using some of: https://gist.github.com/bcap/5682077#file-terminal-control-sh
  ## Can unset with `unset -v {,B,U,I,BI,On_,On_I}{Bla,Red,Gre,Yel,Blu,Pur,Cya,Whi} RCol`
  local RCol='\[\e[0m\]'  # Text Reset

  # Regular          Bold            Underline          High Intensity        BoldHigh Intensity      Background        High Intensity Backgrounds
  local Bla='\[\e[0;30m\]';  local BBla='\[\e[1;30m\]';  local UBla='\[\e[4;30m\]';  local IBla='\[\e[0;90m\]';  local BIBla='\[\e[1;90m\]';  local On_Bla='\e[40m';  local On_IBla='\[\e[0;100m\]';
  local Red='\[\e[0;31m\]';  local BRed='\[\e[1;31m\]';  local URed='\[\e[4;31m\]';  local IRed='\[\e[0;91m\]';  local BIRed='\[\e[1;91m\]';  local On_Red='\e[41m';  local On_IRed='\[\e[0;101m\]';
  local Gre='\[\e[0;32m\]';  local BGre='\[\e[1;32m\]';  local UGre='\[\e[4;32m\]';  local IGre='\[\e[0;92m\]';  local BIGre='\[\e[1;92m\]';  local On_Gre='\e[42m';  local On_IGre='\[\e[0;102m\]';
  local Yel='\[\e[0;33m\]';  local BYel='\[\e[1;33m\]';  local UYel='\[\e[4;33m\]';  local IYel='\[\e[0;93m\]';  local BIYel='\[\e[1;93m\]';  local On_Yel='\e[43m';  local On_IYel='\[\e[0;103m\]';
  local Blu='\[\e[0;34m\]';  local BBlu='\[\e[1;34m\]';  local UBlu='\[\e[4;34m\]';  local IBlu='\[\e[0;94m\]';  local BIBlu='\[\e[1;94m\]';  local On_Blu='\e[44m';  local On_IBlu='\[\e[0;104m\]';
  local Pur='\[\e[0;35m\]';  local BPur='\[\e[1;35m\]';  local UPur='\[\e[4;35m\]';  local IPur='\[\e[0;95m\]';  local BIPur='\[\e[1;95m\]';  local On_Pur='\e[45m';  local On_IPur='\[\e[0;105m\]';
  local Cya='\[\e[0;36m\]';  local BCya='\[\e[1;36m\]';  local UCya='\[\e[4;36m\]';  local ICya='\[\e[0;96m\]';  local BICya='\[\e[1;96m\]';  local On_Cya='\e[46m';  local On_ICya='\[\e[0;106m\]';
  local Whi='\[\e[0;37m\]';  local BWhi='\[\e[1;37m\]';  local UWhi='\[\e[4;37m\]';  local IWhi='\[\e[0;97m\]';  local BIWhi='\[\e[1;97m\]';  local On_Whi='\e[47m';  local On_IWhi='\[\e[0;107m\]';
  ### End Color Vars ### }}}

  _return_error_color=$BRed
  _time_color=$Whi
  if [ `type -t hostColor` == 'function' ]; then
    _user_host_color=`hostColor`
  else
    _user_host_color=$IYel
  fi
  _dir_color=$Yel
  _git_fetch_color=$IYel
  _git_fetch_error_color=$IRed
  _git_branch_color=$Cya
  _git_ahead_color=$Gre
  _git_behind_color=$IRed
  _git_not_tracked_color=$IRed
  _git_modified_color=$Pur
  _git_unknown_color=$Yel
  _git_staged_color=$IBlu
  _git_stashes_color=$IBla
  _git_deleted_color=$Red
  _prompt_color=$IGre
  _console_color=$RCol
}

if [ "$color_prompt" == "yes" ]; then
  _setColors
fi
_setColors

_prompt() {
  local EXIT=$?
  PS1="\[\e]0;\s\]"
  PS1=""

  if ! isTitleFrozen; then
    PS1=$(changeTitle $(basename $SHELL))
  fi

  if [ $EXIT != 0 ]; then
    PS1+="$_return_error_color$EXIT "
  fi

  PS1+="$_time_color\t $_user_host_color\u@\h $_dir_color\w"

  ### Add Git Status ### {{{
  ## Inspired by http://www.terminally-incoherent.com/blog/2013/01/14/whats-in-your-bash-prompt/
  if [[ $(command -v git) ]]; then
    local GStat="$(git status --porcelain -b 2>/dev/null | tr '\n' ':')"

    if [ "$GStat" ]; then
      PS1+=" "

      ### Fetch Time Check ### {{{
      local LAST=$(stat -c %Y $(git rev-parse --git-dir 2>/dev/null)/FETCH_HEAD 2>/dev/null)
      if [ "${LAST}" ]; then
        local TIME=$(($(date +"%s") - ${LAST}))
        ## Check if more than 60 minutes since last
        if [ "${TIME}" -gt "3600" ] && [ ! -e ~/NO_REMOTE ]; then
          ### Don't check if using ssh
          #if [ `git config --get remote.origin.url | cut -d : -f 1` != 'ssh' ]; then
          git fetch 2>/dev/null
          if [ $? -eq 0 ]; then
            PS1+="${_git_fetch_color}↺"
          else
            PS1+="${_git_fetch_error_color}∅"
          fi
          #fi
          ## Refresh var
          local GStat="$(git status --porcelain -b 2>/dev/null | tr '\n' ':')"
        fi
      fi
      ### End Fetch Check ### }}}

      ### Test For Changes ### {{{
      ## Change this to test for 'ahead' or 'behind'!
      #local GChanges="$(echo ${GStat} | tr ':' '\n' | grep -v "^$" | grep -v "^\#\#" | wc -l | tr -d ' ')"
      #if [ "$GChanges" == "0" ]; then
      #  local GitCol=$Gre
      #  else
      #  local GitCol=$Red
      #fi
      ### End Test Changes ### }}}

      PS1+="$_git_branch_color"

      ### Find Branch ### {{{
      #local GBra="$(echo ${GStat} | tr ':' '\n' | grep "^##" | cut -c4- | grep -o "^[a-zA-Z]\{1,\}[^\.]")"
      local GBra="$(echo ${GStat} | tr ':' '\n' | grep "^##" | cut -c4- | sed -re 's/^(.+)\.\.\..+/\1/')"
      if [ "$GBra" ]; then
        #if [ "$GBra" == "master" ]; then
        #  local GBra="M"      # Because why waste space
        #fi
        true
      else
        local GBra="ERROR"      # It could happen supposedly?
      fi
      ### End Branch ### }}}

      PS1+="[$GBra]"  # Add result to prompt

      ### Check for remote tracking branch
      local RemoteBranch=`git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null`

      if [ "$RemoteBranch" == "" ]; then
        PS1+="${_git_not_tracked_color}NT"
      fi

      ### Find Commit Status ### {{{
      ## Test Modified and Untracked for "0"
      # local GDel="$(echo ${GStat} | tr ':' '\n' | grep -c "^[ MARC]D")"
      local GAhe="$(echo ${GStat} | tr ':' '\n' | grep "^##" | grep -o "ahead [0-9]\{1,\}" | grep -o "[0-9]\{1,\}")"
      if [ "$GAhe" ]; then
        PS1+="${_git_ahead_color}↑${GAhe}"  # Ahead
      fi

      ## Needs a `git fetch`
      local GBeh="$(echo ${GStat} | tr ':' '\n' | grep "^##" | grep -o "behind [0-9]\{1,\}" | grep -o "[0-9]\{1,\}")"
      if [ "$GBeh" ]; then
        PS1+="${_git_behind_color}↓${GBeh}"  # Behind
      fi

      local GMod="$(echo ${GStat} | tr ':' '\n' | grep -c "^[ MARC]M")"
      if [ "$GMod" -gt "0" ]; then
        PS1+="${_git_modified_color}≠${GMod}"  # Modified
      fi

      local GDel="$(echo ${GStat} | tr ':' '\n' | grep -c "^ D")"
      if [ "$GDel" -gt "0" ]; then
        PS1+="${_git_deleted_color}⊘${GDel}"  # Deleted
      fi

      local GUnt="$(echo ${GStat} | tr ':' '\n' | grep -c "^\?")"
      if [ "$GUnt" -gt "0" ]; then
        PS1+="${_git_unknown_color}?${GUnt}"  # Untracked
      fi

      local GSta="$(echo ${GStat} | tr ':' '\n' | grep -c "^[MARCD]")"
      if [ "$GSta" -gt "0" ]; then
        PS1+="${_git_staged_color}⍐${GSta}"  # Staged
      fi

      # Count Stashes
      local GStashes="$(git stash list | wc -l)"
      if [ $GStashes != 0 ]; then
        PS1+="${_git_stashes_color}⃞⊡${GStashes}"
      fi
      ### End Commit Status ### }}}
    fi
  else
    MISSING_ITEMS+="git-prompt, "
  fi
  ### End Git Status ### }}}

  PS1+=" "

  ### Check Jobs ### {{{
  command jobs 2>/dev/null
  if [ $? -eq 0 ]; then
    ## Backgrounded running jobs
    local BKGJBS=$(jobs -r | wc -l | tr -d ' ')
    if [ ${BKGJBS} -gt 2 ]; then
      PS1+=" ${Red}[bg:${BKGJBS}]${RCol}"
    elif [ ${BKGJBS} -gt 0 ]; then
      PS1+=" ${Yel}[bg:${BKGJBS}]${RCol}"
    fi

    ## Stopped Jobs
    local STPJBS=$(jobs -s | wc -l | tr -d ' ')
    if [ ${STPJBS} -gt 2 ]; then
      PS1+=" ${Red}[stp:${STPJBS}]${RCol}"
    elif [ ${STPJBS} -gt 0 ]; then
      PS1+=" ${Yel}[stp:${STPJBS}]${RCol}"
    fi
  fi
  ### End Jobs ### }}}

  PS1+="$_prompt_color\$ $_console_color";
}

unset color_prompt force_color_prompt

PROMPT_COMMAND="_prompt"
