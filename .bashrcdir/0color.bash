
cecho() {
  ### Colors to Vars ### {{{
  ## Inspired by http://wiki.archlinux.org/index.php/Color_Bash_Prompt#List_of_colors_for_prompt_and_Bash
  ## Terminal Control Escape Sequences: http://www.termsys.demon.co.uk/vtansi.htm
  ## Consider using some of: https://gist.github.com/bcap/5682077#file-terminal-control-sh
  ## Can unset with `unset -v {,B,U,I,BI,On_,On_I}{Bla,Red,Gre,Yel,Blu,Pur,Cya,Whi} RCol`
  local RCol='\e[0m'  # Text Reset

  # Regular          Bold            Underline          High Intensity        BoldHigh Intensity      Background        High Intensity Backgrounds
  local Bla='\e[0;30m';  local BBla='\e[1;30m';  local UBla='\e[4;30m';  local IBla='\e[0;90m';  local BIBla='\e[1;90m';  local On_Bla='\e[40m';  local On_IBla='\e[0;100m';
  local Red='\e[0;31m';  local BRed='\e[1;31m';  local URed='\e[4;31m';  local IRed='\e[0;91m';  local BIRed='\e[1;91m';  local On_Red='\e[41m';  local On_IRed='\e[0;101m';
  local Gre='\e[0;32m';  local BGre='\e[1;32m';  local UGre='\e[4;32m';  local IGre='\e[0;92m';  local BIGre='\e[1;92m';  local On_Gre='\e[42m';  local On_IGre='\e[0;102m';
  local Yel='\e[0;33m';  local BYel='\e[1;33m';  local UYel='\e[4;33m';  local IYel='\e[0;93m';  local BIYel='\e[1;93m';  local On_Yel='\e[43m';  local On_IYel='\e[0;103m';
  local Blu='\e[0;34m';  local BBlu='\e[1;34m';  local UBlu='\e[4;34m';  local IBlu='\e[0;94m';  local BIBlu='\e[1;94m';  local On_Blu='\e[44m';  local On_IBlu='\e[0;104m';
  local Pur='\e[0;35m';  local BPur='\e[1;35m';  local UPur='\e[4;35m';  local IPur='\e[0;95m';  local BIPur='\e[1;95m';  local On_Pur='\e[45m';  local On_IPur='\e[0;105m';
  local Cya='\e[0;36m';  local BCya='\e[1;36m';  local UCya='\e[4;36m';  local ICya='\e[0;96m';  local BICya='\e[1;96m';  local On_Cya='\e[46m';  local On_ICya='\e[0;106m';
  local Whi='\e[0;37m';  local BWhi='\e[1;37m';  local UWhi='\e[4;37m';  local IWhi='\e[0;97m';  local BIWhi='\e[1;97m';  local On_Whi='\e[47m';  local On_IWhi='\e[0;107m';
  ### End Color Vars ### }}}

  eval "echo -e \${$1}${@:2}\$RCol"
}

LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.cfg=00;32:*.conf=00;32:*.diff=00;32:*.doc=00;32:*.ini=00;32:*.log=00;32:*.patch=00;32:*.pdf=00;32:*.ps=00;32:*.tex=00;32:*.txt=00;32:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:"

hostColor() {
  CHAR=0
  for ((i=0; i<${#HOSTNAME}; i++)); do
    CHAR=$(($CHAR + $(printf '%d' "'${HOSTNAME:$i:1}")))
  done
  echo "\\[\\e[0;9$((CHAR % 9))m\\]"
}
