if [ -d ~/bin ]; then
  addpath ~/bin
fi

if [ -d ~/Development/bin ]; then
  addpath ~/Development/bin
fi

if which yarn >/dev/null; then
  addpath `yarn global bin`
fi
