. <(npm completion)

function nb {
  local binDir=$(npm bin)

  if [ -e $binDir ]; then
    $binDir/$@
  else
    echo No NPM directory
  fi
}

_nb-completion() {
  local cur=${COMP_WORDS[COMP_CWORD]}
  local binDir=$(npm bin)

  if [ -e $binDir ]; then
    COMPREPLY=( $(compgen -W "`ls $binDir`" -- $cur) )
  else
    COMPREPLY=()
  fi
}
complete -F _nb-completion nb

alias nr="npm run"
alias js="jq .scripts package.json"
