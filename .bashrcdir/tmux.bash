function tm {
  readarray -t sessions < <(tmux list-sessions)

  if [ $# -ge 1 ]; then
    if [ "$1" == "." ]; then
      name=$(basename "$(pwd)")
    else
      name=$1
    fi
    if tmux list-sessions 2>&1 | grep "^$name:" >> /dev/null; then
      tmux attach-session -t $name
    else
      tmux new-session \; rename-session $name
    fi
  else
    for i in "${!sessions[@]}"; do
      echo $((i+1)): ${sessions[$i]}
    done

    read -p 'Enter a session number or enter the name for a new session: ' index

    re='^[0-9]+$'
    if ! [[ "$index" =~ $re ]]; then
      tmux new-session \; rename-session $index
    elif [ "$((index - 1))" -ge 0 ] && [ "$((index - 1))" -lt ${#sessions[@]} ]; then
      id=`echo ${sessions[$((index-1))]} | cut -d ':' -f 1`

      tmux attach-session -t $id
    fi
  fi
}

alias stm='tmux source-file ~/.super.tmux.conf'
alias ctm="TMUX='' tm"
alias ctmux="TMUX='' tmux"
