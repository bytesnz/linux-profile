
_shortenFilename() {
  local SHORT=""
  for var in "${@}"; do
    if [[ "$var" != -* ]] && [[ "$var" != +* ]]; then
      # Strip off : stuff
      if [ $1 == "gim" ]; then
        var=$(echo $var | cut -d: -f1)
      fi

      # Strip path of extra directories and extension
      name=$(basename "$var" | sed -re 's/\.[^.]+$//')
      dirname=$(dirname "$var")
      if [[ "$name" != *.* ]] && [ "$dirname" != "." ] && [[ "$name" == index* ]]; then
        name=$(basename "$dirname")/$name
      fi

      if [[ "$SHORT" != *$name* ]]; then
        if [ "$SHORT" == "" ]; then
          SHORT="$name"
        else
          SHORT="$SHORT|$name"
        fi
      fi
    fi
  done
  echo $SHORT
}

isTitleFrozen() {
  if [ ! -z "$STY" ]; then
    currentTitle="$(screen -Q title)"
  elif [ ! -z "$TMUX" ]; then
    currentTitle=$(tmux display-message -p '#W')
  else
    currentTitle="n"
  fi

  if [ "$currentTitle" == "$(basename $SHELL)" ] || [ "${currentTitle:0:1}" == ">" ]; then
    return 1
  else
    return 0
  fi
}

changeTitle() {
  # GNU Screen
  if [ ! -z "$STY" ]; then
    currentTitle=$(screen -Q title)
    if [ "$currentTitle" != "$1" ]; then
      echo -ne "\033k>${1}\033\\"
    fi
  elif [ ! -z "$TMUX" ]; then
    tmux rename-window ">${1}"
  else
    echo -ne "\033]0;>${1}\007";
  fi
}

_changeTitle() {
  local TITLE

  if ! isTitleFrozen; then
    # Vim title
    if [ $1 == "vim" ] || [ $1 == "gim" ]; then
      TITLE="v:$(_shortenFilename ${@:2})"
    elif [ $1 == "lgrep" ]; then # lgrep title
      TITLE="/:$(_shortenFilename ${@:2})"
    elif [ $1 == "npm" ]; then # npm run
      if [ $2 == "start" ]; then
        TITLE="npm"
      elif [ $2 == "run" ]; then
        TITLE="n:$3"
      elif [ $2 == "test" ]; then
        TITLE="n:test"
      fi
    elif [ $1 == "nr" ]; then # npm run
      TITLE="${@:2}"
    elif [ $1 == "nb" ]; then # npm bin
      TITLE="${@:2}"
    elif [ $1 == "git" ]; then # git
      if [ $2 == "diff" ]; then
        if [ $# -gt 2 ]; then
          TITLE="gd:$(_shortenFilename ${@:3})"
        else
          TITLE="git diff"
        fi
      elif [ $2 == "commit" ]; then
        if [ $# -gt 2 ]; then
          TITLE="gc:$(_shortenFilename ${@:3})"
        else
          TITLE="git commit"
        fi
      fi
    fi

    # commands to title
    if [[ " tsc gulp " =~ " $1 " ]]; then
      TITLE=$1
    fi

    # commands to file title
    if [[ " less " =~ " $1 " ]]; then
      TITLE="$1:$(_shortenFilename ${@:2})"
    fi

    if [ "$TITLE" != "" ]; then
      changeTitle "$TITLE"
    fi
  fi
}

trap '_changeTitle $BASH_COMMAND $BASH_ARV' DEBUG
