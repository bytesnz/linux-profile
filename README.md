# linux-profile
The main parts of my customised Linux profile

# Installation
To start using it, clone it into your home directory and add the following
to your `.bashrc` file

```bash
if [ -f ~/.extend.bashrc ]; then
    . ~/.extend.bashrc
fi
```

# Dependencies
There are some functions, aliases and scripts that require dependencies.
Hopefully most of them are listed below:
- `npr` function - Display node package README in lynx
  - npm
  - showdown (npm package)
  - lynx
- `mdhtml` function - Display a markdown file in lynx
  - showdown (npm package)
  - lynx
- prompt - command line prompt
  - git
- `flac2ogg.sh` script - Convert FLAC files into OGG Vorbis files
  - metaflac
  - flac
  - oggenc
- `replaygain.sh` script - Add replaygain to FLAC files
  - metaflac
- `resizer.sh` script
  - convert (part of ImageMagick)
- `nets` & `snets` aliases - Print all TCP/UDP ports that are open and the programs that opened them
  - netstat

# Other Applocations
This repository also includes my(ish) configs for the following applications:
- [GNU Screen](https://www.gnu.org/software/screen)
- [Byobu](byobu.org)
- [abcde](https://abcde.einval.com/wiki)
- [Git](https://git-scm.com)
- [Bash](https://www.gnu.org/software/bash)
- [Vim](https://vim8.org/)
