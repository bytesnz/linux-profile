#!/bin/bash
if [ $# -lt 2 ]; then
	echo Must have only two arguments
	echo "`basename $0` <output_dir> <base_dir>"
	exit 1
fi

argn=$#

function doconvert {
  path=`readlink -e "$1"`
  echo "looking for files in ($1) $path"
  find "$path" -name '*.flac' -printf '%p\n' | while read file; do
    # Check folder exists
    echo file is $file

    if [ $argn -ge 2 ]; then
      file=${file/.flac/}
      #echo expanded file is $file
      echo "removing $base_dir from $file"
      output=${file/$base_dir\//}
      echo $output
      output="$output_dir/$output.ogg"
    else
      file=${file/.flac/}
      output="$output_dir/$file.ogg"
    fi

    echo Output is $output

    #continue

    if [ ! -e "$output" ]; then
      dir=`dirname "$output"`
      if [ ! -d "$dir" ]; then
        mkdir -p "$dir"
      fi

      echo "$((i++))/$num_files $file.flac > $output"
      error=0

      # Check if we have replaygain
      metaflac --export-tags-to=- "$file.flac" | grep -q REPLAYGAIN
      if [ $? -ne 0 ]; then
        >&2 echo "!!!! $file.flac: Does not seem to have replay gain!!!!!"
        replay=""
      else
        replay="--apply-replaygain-which-is-not-lossless=0tln1"
      fi

      title=`metaflac --show-tag="title" "$file.flac" | sed "s/TITLE=//i"`
      if [ "$title" == "" ]; then
        echo "$file.flac does not have a title"
        error=$((error+1))
      fi
      artist=`metaflac --show-tag="artist" "$file.flac" | sed "s/ARTIST=//i"`
      if [ "$artist" == "" ]; then
        echo "$file.flac does not have a artist"
        error=$((error+1))
      fi
      album=`metaflac --show-tag="album" "$file.flac" | sed "s/ALBUM=//i"`
      if [ "$album" == "" ]; then
        echo "$file.flac does not have a album"
        error=$((error+1))
      fi
      date=`metaflac --show-tag="date" "$file.flac" | sed "s/DATE=//i"`
      genre=`metaflac --show-tag="genre" "$file.flac" | sed "s/GENRE=//i"`
      track=`metaflac --show-tag="tracknumber" "$file.flac" | sed "s/TRACKNUMBER=//i"`
      if [ "$track" == "" ]; then
        echo "$file.flac does not have a track number"
        error=$((error+1))
      fi

      if [ $error -ne 0 ]; then
        >&2 echo $file.flac: Missing $error bits of information, skipping
        echo $file.flac: Missing $error bits of information, skipping
        continue;
      fi

      flac -c --apply-replaygain-which-is-not-lossless=0tln1 -d "$file.flac" | oggenc -t "$title" -a "$artist" -G "$genre" -l "$album" \
       -d "$date" -N "$track" -o "$output" -q 7 -

      if [ $? -ne 0 ]; then
        echo "Error converting file. Stopping"
        return 1
      fi
    else
      test $((i++))
    fi
  done
}

output_dir=`readlink -e "$1"`
if [ $# -ge 2 ]; then
	base_dir=`readlink -e "$2"`
fi

echo "Base dir is $base_dir, output dir is $output_dir"

if [ $# -gt 2 ]; then
  for f in "${@:3}"; do
    doconvert "$f"
  done
else
  doconvert "$2"
fi
