#!/bin/bash

cwd=`pwd`
if [ $# -eq 1 ]; then
	cd $1
fi

find . -name '*.flac' | while read file; do
	dirname "$file"
done | sort -u | while read dir; do
	echo "Adding replay gain to $dir"
	cd "$dir"
	metaflac --add-replay-gain *.flac
	cd "$cwd"
done
