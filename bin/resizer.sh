dir=/mnt/data/home/jack/resized

cwd=`pwd`

regex='.*\/[^-][^-].*\.JPG';

# Delete deleted images
cd "$dir"
#find . ! -path '*--*' -iname '*.jpg' | while read file; do
find . ! -path '*--*' -iregex $regex | while read file; do
	if [ ! -f "$cwd/$file" ]; then
		echo $file is no longer there, deleting
		rm "$file"
	fi
done

cd "$cwd"

# Resize images
#find . ! -path '*--*' -iname '*.jpg' | while read file; do
find . ! -path '*--*' -iregex $regex | while read file; do

	path=`dirname "$file"`
	name=`basename "$file"`

	if [ ! -d "$dir/$path" ]; then
		mkdir -p "$dir/$path"
	fi

	if [ ! -f "$dir/$path/$name" ]; then
		# convert -auto-orient -resize 1100x1100 -quality 80 "$file" "$dir/$path/$name"
		# Convert so smallest size is 740 pixels
		convert -auto-orient -resize '740^>' -quality 80 "$file" "$dir/$path/$name"
	
		echo Done $file
	else
		echo Skipping $file
	fi
done
